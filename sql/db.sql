CREATE DATABASE taskmanager
    WITH
    OWNER = tm
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

GRANT TEMPORARY, CONNECT ON DATABASE taskmanager TO PUBLIC;

GRANT ALL ON DATABASE taskmanager TO tm;BASE "task-manager" TO tm;