package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.LogDto;

public interface ILoggerService {

    void writeLog(@NotNull LogDto message);

}