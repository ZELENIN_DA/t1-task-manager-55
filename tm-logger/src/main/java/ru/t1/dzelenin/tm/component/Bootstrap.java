package ru.t1.dzelenin.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.api.service.IReceiverService;
import ru.t1.dzelenin.tm.listener.LogListener;
import ru.t1.dzelenin.tm.service.ReceiverService;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private LogListener logListener;

    @SneakyThrows
    public void init() {
        receiverService.receive(logListener);
    }

}
