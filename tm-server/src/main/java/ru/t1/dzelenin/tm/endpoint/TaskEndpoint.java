package ru.t1.dzelenin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dzelenin.tm.api.service.IServiceLocator;
import ru.t1.dzelenin.tm.api.service.dto.ITaskDTOService;
import ru.t1.dzelenin.tm.dto.model.SessionDTO;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;
import ru.t1.dzelenin.tm.dto.request.task.*;
import ru.t1.dzelenin.tm.dto.response.task.*;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.enumerated.TaskSort;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.dzelenin.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskDTOService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final TaskDTO task = getTaskService().changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Date dateBegin = request.getDateBegin();
        @Nullable final Date dateEnd = request.getDateBegin();
        getTaskService().create(userId, name, description, dateBegin, dateEnd);
        return new TaskCreateResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final TaskSort sort = request.getSort();
        @NotNull final List<TaskDTO> tasks = getTaskService().findAll(userId, sort.getComparator());
        return new TaskListResponse(tasks);
    }


    @NotNull
    @Override
    @WebMethod
    public TaskListByProjectIdResponse listTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListByProjectIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @NotNull final List<TaskDTO> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = getTaskService().findOneById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

}