package ru.t1.dzelenin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.dzelenin.tm.comporator.CreatedComparator;
import ru.t1.dzelenin.tm.comporator.DateBeginComparator;
import ru.t1.dzelenin.tm.comporator.StatusComparator;
import ru.t1.dzelenin.tm.dto.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateBeginComparator.INSTANCE) return "status";
        else return "name";
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.remove(model);
    }

}