package ru.t1.dzelenin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.service.IAuthService;
import ru.t1.dzelenin.tm.api.service.IPropertyService;
import ru.t1.dzelenin.tm.api.service.dto.IUserDTOService;
import ru.t1.dzelenin.tm.dto.model.SessionDTO;
import ru.t1.dzelenin.tm.dto.model.UserDTO;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.exception.field.LoginEmptyException;
import ru.t1.dzelenin.tm.exception.field.PasswordEmptyException;
import ru.t1.dzelenin.tm.exception.user.AccessDeniedException;
import ru.t1.dzelenin.tm.util.CryptUtil;
import ru.t1.dzelenin.tm.util.HashUtil;

import java.util.Date;


public class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserDTOService userService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserDTOService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null || user.getLocked()) throw new AccessDeniedException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull String hash = HashUtil.salt(password, secret, iteration);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return getToken(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO session = objectMapper.readValue(json, SessionDTO.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    @Override
    public UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO user) {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return session;
    }

}
