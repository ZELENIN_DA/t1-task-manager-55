package ru.t1.dzelenin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;

public interface IProjectTaskDTOService {

    void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void removeProjectById(@NotNull String userId, @NotNull String projectId);

}