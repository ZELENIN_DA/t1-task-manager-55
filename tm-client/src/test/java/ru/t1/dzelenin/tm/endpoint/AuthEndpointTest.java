package ru.t1.dzelenin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dzelenin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dzelenin.tm.dto.request.user.UserLoginRequest;
import ru.t1.dzelenin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dzelenin.tm.dto.request.user.UserProfileRequest;
import ru.t1.dzelenin.tm.dto.response.user.UserLoginResponse;
import ru.t1.dzelenin.tm.dto.response.user.UserLogoutResponse;
import ru.t1.dzelenin.tm.dto.response.user.UserProfileResponse;
import ru.t1.dzelenin.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {
/*
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Test
    public void login() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(
                null, null)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(
                "", "")));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(
                "wrongLogin", "wrongPassword")));

        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(new UserLoginRequest(
                "1", "1"));
        Assert.assertNotNull(userLoginResponse);

        @Nullable final String token = userLoginResponse.getToken();
        Assert.assertNotNull(token);
    }

    @Test
    public void logout() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest()));
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest(null)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest("wrongToken")));

        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(new UserLoginRequest("1", "1"));
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        Assert.assertTrue(userLoginResponse.getSuccess());

        @NotNull final UserLogoutResponse userLogoutResponse = authEndpoint.logout(new UserLogoutRequest(userLoginResponse.getToken()));
        Assert.assertNotNull(userLogoutResponse);
    }

    @Test
    public void profile() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest()));
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest(null)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest("wrongLogin")));

        @Nullable final UserLoginResponse userLoginResponse = authEndpoint.login(new UserLoginRequest("1", "1"));
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        Assert.assertTrue(userLoginResponse.getSuccess());

        @Nullable final UserProfileResponse userProfileResponse = authEndpoint.profile(new UserProfileRequest(userLoginResponse.getToken()));
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
    }
*/
}
