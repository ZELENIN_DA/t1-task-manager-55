package ru.t1.dzelenin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dzelenin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dzelenin.tm.api.endpoint.IUserEndpoint;
import ru.t1.dzelenin.tm.dto.request.user.*;
import ru.t1.dzelenin.tm.dto.response.user.*;
import ru.t1.dzelenin.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {
/*
    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void init() {
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        adminToken = adminResponse.getToken();

        @NotNull final UserLoginResponse userResponse = authEndpoint.login(
                new UserLoginRequest("1", "1")
        );
        userToken = userResponse.getToken();
    }

    @Test
    public void changeUserPassword() {
        final String newPassword = "5";

        Assert.assertThrows(Exception.class, () -> userEndpoint.changePassword(new UserChangePasswordRequest()));

        @Nullable final UserChangePasswordRequest request = new UserChangePasswordRequest(userToken, "1");
        request.setPassword(newPassword);

        @Nullable final UserChangePasswordResponse response = userEndpoint.changePassword(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());

        @Nullable final UserLoginResponse loginCheck = authEndpoint.login(new UserLoginRequest("1", newPassword));
        Assert.assertTrue(loginCheck.getSuccess());

        request.setPassword("1");
        @Nullable final UserChangePasswordResponse undoResponse = userEndpoint.changePassword(request);
        Assert.assertNotNull(undoResponse);
        Assert.assertNotNull(undoResponse.getUser());
    }

    @Test
    public void lockUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(null, null)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest("", "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest("wrongToken", "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(userToken, "wrongLogin")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(userToken, "1")));

        @Nullable final UserLockResponse response = userEndpoint.lockUser(new UserLockRequest(adminToken, "admin"));
        Assert.assertNotNull(response);

        @Nullable final UserProfileResponse userProfileResponse = authEndpoint.profile(new UserProfileRequest(userToken));
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
        Assert.assertTrue(userProfileResponse.getUser().isLocked());
        userEndpoint.unlockUser(new UserUnlockRequest(adminToken, "1"));
    }

    @Test
    public void unlockUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(null, null)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest("", "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest("wrongToken", "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(userToken, "wrongLogin")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(userToken, "2")));

        @Nullable final UserLockResponse lockResponse = userEndpoint.lockUser(new UserLockRequest(adminToken, "admin"));
        Assert.assertNotNull(lockResponse);

        @Nullable final UserUnlockResponse unlockResponse = userEndpoint.unlockUser(new UserUnlockRequest(adminToken, "admin"));
        Assert.assertNotNull(unlockResponse);

        @Nullable final UserProfileResponse userProfileResponse = authEndpoint.profile(new UserProfileRequest(userToken));
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
        Assert.assertFalse(userProfileResponse.getUser().isLocked());
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(
                null, "1", "1", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(
                "", "1", "1", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(
                "qwe", "1", "1", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(
                userToken, null, "1", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(
                userToken, "1", null, "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(
                userToken, "1", "1", null)
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(
                userToken, "1", "1", "email")
        ));

        @Nullable final UserRegistryResponse response =
                userEndpoint.registryUser(new UserRegistryRequest(userToken, "temp", "pass", "emails"));
        Assert.assertNotNull(response.getUser());

        @Nullable final User user = response.getUser();
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("temp", user.getLogin());

        @Nullable final UserLoginResponse login = authEndpoint.login(new UserLoginRequest("temp", "pass"));
        Assert.assertTrue(login.getSuccess());
    }

    @Test
    public void removeUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(null, null)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest("", null)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest("wrongToken", null)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(userToken, null)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(adminToken, null)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(adminToken, "")));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(adminToken, "wrongLogin")));

        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("temp", "pass"));
        Assert.assertTrue(loginResponse.getSuccess());

        @Nullable final UserRemoveResponse response = userEndpoint.removeUser(new UserRemoveRequest(adminToken, "temp"));
        Assert.assertNotNull(response);
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("temp", "pass")));

    }

    @Test
    public void updateUserProfile() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateProfileUser(
                new UserUpdateProfileRequest())
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateProfileUser(
                new UserUpdateProfileRequest(null, "1", "2", "3")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateProfileUser(
                new UserUpdateProfileRequest("", "1", "2", "3")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateProfileUser(
                new UserUpdateProfileRequest("token", "1", "2", "3")
        ));

        @Nullable final UserUpdateProfileResponse response = userEndpoint.updateProfileUser(
                new UserUpdateProfileRequest(userToken, "update", "profile", "user")
        );
        Assert.assertNotNull(response.getUser());

        @Nullable final User user = response.getUser();
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("profile", user.getLastName());

        userEndpoint.updateProfileUser(new UserUpdateProfileRequest(
                userToken, "user", "return", "value"
        ));
    }
*/
}
