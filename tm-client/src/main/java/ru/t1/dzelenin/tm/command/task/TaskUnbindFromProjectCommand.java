package ru.t1.dzelenin.tm.command.task;

import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.dzelenin.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken(), projectId, taskId);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getProjectTaskEndpoint().unbindTaskFromProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unbind task from project.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
