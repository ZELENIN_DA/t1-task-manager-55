package ru.t1.dzelenin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.api.model.ICommand;
import ru.t1.dzelenin.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ApplicationListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (ICommand command : commands) {
            if (command == null) continue;
            @NotNull final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name.toString());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Show command list.";
    }

}
