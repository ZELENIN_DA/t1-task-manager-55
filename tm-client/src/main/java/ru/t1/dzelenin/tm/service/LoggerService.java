package ru.t1.dzelenin.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dzelenin.tm.api.service.ILoggerService;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.*;

@Service
@NoArgsConstructor
public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String LOG_CATALOG_NAME = "log";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMAND_FILE = String.format("./%s/commands.xml", LOG_CATALOG_NAME);

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = String.format("./%s/errors.xml", LOG_CATALOG_NAME);

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = String.format("./%s/messages.xml", LOG_CATALOG_NAME);

    @NotNull
    private static final LogManager MANAGER = LogManager.getLogManager();

    @NotNull
    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    @Getter
    @NotNull
    private static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);

    @Getter
    @NotNull
    private static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);

    @Getter
    @NotNull
    private static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);

    @NotNull
    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    static {
        logConfigFromFile();
        registry(LOGGER_COMMAND, COMMAND_FILE, false);
        registry(LOGGER_ERROR, ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGES_FILE, true);
    }

    private static void logConfigFromFile() {
        try {
            @NotNull final Path logPath = Paths.get("./", LOG_CATALOG_NAME);
            if (!Files.exists(logPath)) Files.createDirectory(logPath);
            @NotNull final Class<?> clazz = LoggerService.class;
            @Nullable final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE);
            MANAGER.readConfiguration(inputStream);
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @NotNull
    private static ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private static void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

}
