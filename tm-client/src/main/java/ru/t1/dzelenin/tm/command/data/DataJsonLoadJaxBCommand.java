package ru.t1.dzelenin.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.dto.request.domain.DataJsonLoadJaxBRequest;
import ru.t1.dzelenin.tm.enumerated.Role;

@Component
public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-load-json-jaxb";

    @Getter
    @NotNull
    private final String description = "Load data from json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        getDomainEndpoint().loadDataJsonJaxb(new DataJsonLoadJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}


