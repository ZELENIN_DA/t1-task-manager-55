package ru.t1.dzelenin.tm.command.task;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.exception.entity.TaskNotFoundException;

@Component
public abstract class AbstractTaskShowCommand extends AbstractTaskCommand {

    protected void showTask(final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
