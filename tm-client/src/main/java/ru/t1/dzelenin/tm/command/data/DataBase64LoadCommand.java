package ru.t1.dzelenin.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.dto.request.domain.DataBase64LoadRequest;
import ru.t1.dzelenin.tm.enumerated.Role;

@Component
public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @Getter
    @NotNull
    private final String description = "Load data from base64 file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

