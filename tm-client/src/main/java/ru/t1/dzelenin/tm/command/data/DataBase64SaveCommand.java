package ru.t1.dzelenin.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.dto.request.domain.DataBase64SaveRequest;
import ru.t1.dzelenin.tm.enumerated.Role;

@Component
public final class DataBase64SaveCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-save-base64";

    @Getter
    @NotNull
    private final String description = "Save data to BASE64 file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        getDomainEndpoint().saveDataBase64(new DataBase64SaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}


