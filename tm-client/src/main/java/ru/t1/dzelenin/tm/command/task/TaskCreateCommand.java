package ru.t1.dzelenin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.dto.request.task.TaskCreateRequest;
import ru.t1.dzelenin.tm.util.TerminalUtil;

import java.util.Date;

@Component
public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("[ENTER DATE BEGIN:]");
        @Nullable final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("[ENTER DATE END:]");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken(), name, description, dateBegin, dateEnd);
        getTaskEndpoint().createTask(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
