package ru.t1.dzelenin.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.api.model.ICommand;
import ru.t1.dzelenin.tm.api.service.IServiceLocator;
import ru.t1.dzelenin.tm.api.service.ITokenService;
import ru.t1.dzelenin.tm.enumerated.Role;

@Getter
@Setter
@Component

public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    protected ITokenService tokenService;


    public abstract void execute();

    @Nullable
    public abstract String getName();

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @Nullable
    public abstract String getDescription();

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
