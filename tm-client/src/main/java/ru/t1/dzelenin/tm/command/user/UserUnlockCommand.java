package ru.t1.dzelenin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.dto.request.user.UserUnlockRequest;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.util.TerminalUtil;

@Component
public final class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserEndpoint().unlockUser(new UserUnlockRequest(getToken(), login));
    }

    @NotNull
    @Override
    public String getName() {
        return "user-unlock";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getDescription() {
        return "user unlock";
    }

}
