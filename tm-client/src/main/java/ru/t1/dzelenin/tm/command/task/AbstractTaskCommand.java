package ru.t1.dzelenin.tm.command.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.api.endpoint.IProjectTaskEndpoint;
import ru.t1.dzelenin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dzelenin.tm.command.AbstractCommand;
import ru.t1.dzelenin.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Autowired
    private IProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

