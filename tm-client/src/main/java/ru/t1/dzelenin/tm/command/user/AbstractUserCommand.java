package ru.t1.dzelenin.tm.command.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dzelenin.tm.api.endpoint.IUserEndpoint;
import ru.t1.dzelenin.tm.command.AbstractCommand;

@Getter
@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @Autowired
    private IUserEndpoint userEndpoint;

    @Autowired
    private IAuthEndpoint authEndpoint;

}