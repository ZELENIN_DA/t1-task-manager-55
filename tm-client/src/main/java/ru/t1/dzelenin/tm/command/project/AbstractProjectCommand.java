package ru.t1.dzelenin.tm.command.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dzelenin.tm.command.AbstractCommand;
import ru.t1.dzelenin.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @Autowired
    private IProjectEndpoint projectEndpoint;


    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

