package ru.t1.dzelenin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        getTaskEndpoint().changeTaskStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
