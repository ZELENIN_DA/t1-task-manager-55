package ru.t1.dzelenin.tm.command.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.api.endpoint.IDomainEndpoint;
import ru.t1.dzelenin.tm.command.AbstractCommand;

@NoArgsConstructor
@Getter
@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @Autowired
    private IDomainEndpoint domainEndpoint;

}