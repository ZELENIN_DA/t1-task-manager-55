package ru.t1.dzelenin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dzelenin.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.dzelenin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskEndpoint().removeTaskById(new TaskRemoveByIdRequest(getToken(), id));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by id.";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
