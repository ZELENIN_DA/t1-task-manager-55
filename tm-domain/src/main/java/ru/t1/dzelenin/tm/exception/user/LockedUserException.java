package ru.t1.dzelenin.tm.exception.user;

public class LockedUserException extends AbstractUserException {

    public LockedUserException() {
        super("Error! User is locked...");
    }

}
